/**
 * @file
 * SureTax javascript functions.
 */

Drupal.behaviors.suretax = {
    attach: function (context) {
        var selected = jQuery('select[name="commerce_line_items[und][actions][line_item_type]"] option:selected').val();
        if (selected === 'suretax_calculation') {
            window.location.reload();
        }
    }
};