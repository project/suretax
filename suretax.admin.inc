<?php

/**
 * @file
 * API configuration form for suretax module.
 */

/**
 * 
 * Implements Suretax admin UI form.
 */
function suretax_admin_form($form, $form_state) {
  $form = array();
  $form['suretax_checkout_name'] = array(
    '#title' => t('SureTax Name'),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#description' => t('Enter SureTax name that you would like to display in Checkout page.'),
    '#default_value' => variable_get('suretax_checkout_name', ''),
  );
  $specifications = array(
    'general' => t('General Sales , Communications or Utilities Transaction'),
  );
  $form['suretax_type'] = array(
    '#title' => t('SureTax API Specification'),
    '#type' => 'radios',
    '#options' => $specifications,
    '#default_value' => variable_get('suretax_type', 'general'),
    '#required' => TRUE,
  );
  $mode = variable_get('suretax_mode', 'Development');
  if (isset($form_state['values']['suretax_mode']) && $form_state['values']['suretax_mode']) {
    $mode = $form_state['values']['suretax_mode'];
  }
  $form['suretax_credentials'] = array(
    '#type' => 'fieldset',
    '#title' => t('Suretax Credentials'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['suretax_credentials']['suretax_mode'] = array(
    '#title' => t('Select Mode'),
    '#type' => 'select',
    '#options' => drupal_map_assoc(array(t('Development'), t('Live'))),
    '#required' => TRUE,
    '#description' => 'Select Live mode only in Production Environment',
    '#default_value' => $mode,
    '#ajax' => array(
      'callback' => 'suretax_mode_callback',
      'wrapper' => 'suretax-credentials',
    ),
  );
  $form['suretax_credentials']['data'] = array(
    '#type' => 'container',
    '#prefix' => '<div id ="suretax-credentials">',
    '#suffix' => '</div>',
  );
  $form['suretax_credentials']['data']['suretax_client_id_' . $mode] = array(
    '#title' => t('Client ID'),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#description' => t('Enter Client Id for @mode site', array('@mode' => $mode)),
    '#default_value' => variable_get('suretax_client_id_' . $mode, ''),
    '#size' => 10,
    '#maxlength' => 10,
  );
  $form['suretax_credentials']['data']['suretax_validation_key_' . $mode] = array(
    '#title' => t('Validation Key'),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#description' => t('Enter License Key for @mode site', array('@mode' => $mode)),
    '#default_value' => variable_get('suretax_validation_key_' . $mode, ''),
    '#size' => 36,
    '#maxlength' => 36,
  );

  return system_settings_form($form);
}

/**
 * Implements ajax callback to change mode.
 */
function suretax_mode_callback($form, &$form_state) {
  return $form['suretax_credentials']['data'];
}
